'use strict'

const navMobile = document.querySelector('.nav__mobile'),
      navItem = document.querySelector('.nav__item');

navMobile.addEventListener('click', () =>{
    navMobile.classList.toggle('nav__item_active');
})